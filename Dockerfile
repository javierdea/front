
# de que imagen parto
FROM node

#carpeta raiz donde se quiere que vaya la aplicacion
WORKDIR /frontmiapp

#copia de archivos a la carpeta destino donde correra el contenedor de la carpeta local
ADD . /frontmiapp

#instalo los paquetes necesarios para que se encarguen de instalar el node_modules, ya que en el fichero .dockerignore le he dicho que me ignore esta carpeta

RUN npm install -g bower


#Volumen de la imagen, al ejecutar le decimos donde va a mapear
VOLUME ["/logs"]

# puerto que expone tu imagen destino, por el que se tiene que conectar
EXPOSE 8081

#comando de iniciado cuando arranque el contenedor
CMD ["polymer", "serve"]
